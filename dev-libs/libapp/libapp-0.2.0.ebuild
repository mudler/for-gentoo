# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4
inherit eutils multilib
DESCRIPTION="libapp provides functions to easily perform some often needed operations in application development, such as command line parsing."
HOMEPAGE="https://github.com/drotiro/libapp"
SRC_URI="https://github.com/drotiro/${PN}/archive/${PV}.tar.gz"
LICENSE="GPL3"
SLOT="0.2.0"
KEYWORDS="~x86 ~amd64"
IUSE=""
DEPEND=""
RDEPEND="${DEPEND}"
S="${WORKDIR}/${PN}"

src_unpack(){
	unpack ${A}
	mv ${PN}-${PV} ${PN}
}

src_install() {
	dodir /usr
	dodir /usr/lib
	emake INSTALL_S="install" PREFIX="${D}/usr" install
	dodoc -r test/
}
